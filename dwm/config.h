/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int fillgrid  = 1;        /* make grid layout fill the whole screen */
static const int default_hfr        = 20;
static const int gap_toggle[]     = {0, 10, 30, 60, 90, 120};
static const unsigned int snap      = 12;       /* snap pixel */
static const int title_scheme       = 0;        /* 1 means color of window title in top bar has SelScheme */
static const int single_bt          = 0;        /* show border only on top in nonocle and 1 window tile */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static int ralign                   = 0;        /* 0 means windows start stackin on the left side */

static const char *fonts[]          = { "Terminus:style=Regular:pixelsize=10:antialias=true:autohint=true" };
static const char dmenufont[]       = "Terminus:style=Regular:pixelsize=10:antialias=true:autohint=true";
//static const char *fonts[]          = { "monospace:size=10" };
//static const char dmenufont[]       = "monospace:size=10";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#333333";
static const char col_gray3[]       = "#444444";
static const char col_gray4[]       = "#555555";
static const char col_gray5[]       = "#666666";
static const char col_gray6[]       = "#777777";
static const char col_gray7[]       = "#999999";
static const char col_gray8[]       = "#bbbbbb";
static const char col_gray9[]       = "#cccccc";
static const char col_gray10[]      = "#dddddd";
static const char col_gray11[]      = "#eeeeee";
static const char col_cyan[]        = "#005577";

/* solarized colors http://ethanschoonover.com/solarized */
static const char s_base03[]        = "#002b36";
static const char s_base02[]        = "#073642";
static const char s_base01[]        = "#586e75";
static const char s_base00[]        = "#657b83";
static const char s_base0[]         = "#839496";
static const char s_base1[]         = "#93a1a1";
static const char s_base2[]         = "#eee8d5";
static const char s_base3[]         = "#fdf6e3";

/* OTHER COLORS */
//static const char normbordercolor[]   = "#171717";
//static const char normbgcolor[]       = "#171717";
static const char normbordercolor[]   = "#000000";
static const char normbgcolor[]       = "#000000";
static const char normfgcolor[]       = "#b2a191";

static const char selbordercolor[]    = "#627a92";
static const char selbgcolor[]        = "#bf3f34";
static const char selfgcolor[]        = "#e8d0c3";

static const char unselbgcolor[]      = "#171717";
static const char unselfgcolor[]      = "#b2a191";

static const char titlebgcolor[]      = "#171717";
static const char titlefgcolor[]      = "#b8ca4b";

static const char ltsymbgcolor[]      = "#171717";
static const char ltsymfgcolor[]      = "#e8d0c3";

static const char highlightcolor[]    = "#e8d0c3";

static const char halo[] = "#555555";


static const char *colors[][3]      = {
	/*fg         bg         border   */
	{ col_gray3, col_gray1, col_gray2 },
	{ col_gray4, col_cyan,  col_cyan  },

    [SchemeNorm] = { normfgcolor  , normbgcolor  , col_gray1 } ,
    //[SchemeSel]  = { titlefgcolor , titlebgcolor , col_gray2 } ,
    [SchemeSel]  = { titlefgcolor , titlebgcolor , halo } ,

	{ s_base0, s_base03, s_base2 },      /* SchemeNorm dark */
	{ s_base0, s_base02, s_base2 },      /* SchemeSel dark */
	{ s_base00, s_base3, s_base02 },     /* SchemeNorm light */
	{ s_base00, s_base2, s_base02},      /* SchemeSel light */
	{ col_gray3, col_gray1, col_gray2 }, /* SchemeNorm orig */
	{ col_gray4, col_cyan,  col_cyan  }, /* SchemeSel orig */
};

/* tagging */
static const int bar_tags_y_offset = -1;
//static const char *tags[] = { "•", "•", "•", "•", "•", "•", "•", "•", "•" };
//static const char *tags[] = { "◤", "◤", "◤", "◤", "◤", "◤", "◤", "◤", "◤" };
//static const char *tags[] = { "■", "□", "▢", "▣", "▤", "▥", "▦", "▧", "▩"};
//static const char *tags_pick = "■□▢▣▤▥▦▧▨▩•▪▫▬▭▮▯◘▱▲△▴▵▶▷▸▹►▻▼▽▾▿◀◁◂◃◄◅◆◇◈◉◊○◌◍◎●◐◑◒◓◔◕◖◗◘◙◚◛◜◝◞◟◠◡◢◣◤◥◦◧◨◩◪◫◬◭◮◯◰◱◲◳◴◵◶◷◸◹◺◻◼◿";
//static const char *tags[] = { "𝟙", "𝟚", "𝟛", "𝟜", "𝟝", "𝟞", "𝟟", "𝟠", "𝟡" };
//static const char *tags[] = { "𝟣", "𝟤", "𝟥", "𝟦", "𝟧", "𝟨", "𝟩", "𝟪", "𝟫" };
//static const char *tags[] = { "𝟭", "𝟮", "𝟯", "𝟰", "𝟱", "𝟲", "𝟳", "𝟴", "𝟵" };
//static const char *tags[] = { "𝟷", "𝟸", "𝟹", "𝟺", "𝟻", "𝟼", "𝟽", "𝟾", "𝟿" };
//static const char *tags[] = { "•", "•", "•", "◦", "◦", "◦", "•", "•", "•" };
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "DUMB_FILLER" , NULL , NULL , 0      , 1 , -1 } ,
	//{ "Gimp"        , NULL , NULL , 0      , 1 , -1 } ,
	//{ "Firefox"     , NULL , NULL , 1 << 8 , 0 , -1 } ,
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const int default_layout = 0;
static const int default_combined_layout = 4;
static const Layout layouts[] = {
	/* symbol , mfact , gap_o gap_i , nmaster , arrange function */
	{ "[]="   , 0.5   , 0   , 0     , 1       , tile    } , /* first entry is default */
	{ "><>"   , 0     , 0   , 0     , 1       , NULL    } , /* no layout function means floating behavior */
	{ "[M]"   , 0.90  , 0   , 0     , 1       , monocle } ,
	{ "<3"    , 0.40  , 90  , 10    , 3       , nicelt  } ,
	{ "#"     , 0     , 30  , 30    , 0       , gridlt  } ,
	{ "i3"    , 0     , 0   , 0     , 0       , i3      } ,
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	/*{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} },*/ \
	{ MODKEY|ShiftMask,             KEY,      tagandview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
//static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *dmenucmd[]  = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, NULL };
static const char *dmenu_open_cmd[]  = { "dmenu_open", NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *xbacklight_up[]   =      { "xbacklight", "+10" , NULL } ;
static const char *xbacklight_down[] =      { "xbacklight", "-10" , NULL } ;
static const char *xbacklight_up_tiny[]   = { "xbacklight", "+1" , NULL  } ;
static const char *xbacklight_down_tiny[] = { "xbacklight", "-1" , NULL  } ;
static const char *volume_mute_toggle[] = { "amixer", "-q", "sset", "Master,0", "toggle", NULL       } ;
static const char *volume_up[]   =        { "amixer", "-q", "sset", "Master,0", "5+", "unmute", NULL } ;
static const char *volume_down[] =        { "amixer", "-q", "sset", "Master,0", "5-", "unmute", NULL } ;
static const char *volume_up_tiny[]   =   { "amixer", "-q", "sset", "Master,0", "1+", "unmute", NULL } ;
static const char *volume_down_tiny[] =   { "amixer", "-q", "sset", "Master,0", "1-", "unmute", NULL } ;

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_p,      spawn,          {.v = dmenu_open_cmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setfact,        {.f = -0.0125} },
	{ MODKEY,                       XK_l,      setfact,        {.f = +0.0125} },
	{ MODKEY,                       XK_o,      zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_c,      togglecollapse, {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_Delete, killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_n,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_e,      setlayout,      {.v = &layouts[4]} },
	{ MODKEY,                       XK_v,      setlayout,      {.v = &layouts[5]} },
	{ MODKEY|ShiftMask,             XK_v,      togglevertical, {0} },
	//{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY,                       XK_space,  togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {.i = 1} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_h,      viewside,       {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_l,      viewside,       {.i = 1 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -5 } },
	{ MODKEY,                       XK_plus,   setgaps,        {.i = +5 } },
	{ MODKEY|ShiftMask,             XK_minus,  setgaps_i,      {.i = -5 } },
	{ MODKEY|ShiftMask,             XK_plus,   setgaps_i,      {.i = +5 } },
	{ MODKEY,                       XK_g,      togglegaps,     {0} },
	{ MODKEY|ShiftMask,             XK_g,      togglegaps_i,   {0} },
	{ MODKEY|ControlMask,           XK_j,      changeheight,   {.i = +4 } },
	{ MODKEY|ControlMask,           XK_k,      changeheight,   {.i = -4 } },
	{ MODKEY|ControlMask,           XK_r,      changeheight,   {.i = 0 } },
	{ MODKEY|ControlMask,           XK_l,      setwidth,       {.i = +25 } },
	{ MODKEY|ControlMask,           XK_h,      setwidth,       {.i = -25 } },
	{ MODKEY|ControlMask|ShiftMask, XK_l,      setxpos,        {.i = +25 } },
	{ MODKEY|ControlMask|ShiftMask, XK_h,      setxpos,        {.i = -25 } },
	{ MODKEY|ControlMask|ShiftMask, XK_j,      setypos,        {.i = +25 } },
	{ MODKEY|ControlMask|ShiftMask, XK_k,      setypos,        {.i = -25 } },
	{ MODKEY,                       XK_w,      centerwindow,   {0} },
	{ MODKEY|ShiftMask,             XK_w,      cornerwindow,   {.i = 3} },
	{ MODKEY,                       XK_r,      toggleralign,   {0} },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
    /* media buttons */
    {MODKEY,                        XK_F12,    spawn,          {.v = xbacklight_up        }},
    {MODKEY,                        XK_F11,    spawn,          {.v = xbacklight_down      }},
    {MODKEY|ShiftMask,              XK_F12,    spawn,          {.v = xbacklight_up_tiny   }},
    {MODKEY|ShiftMask,              XK_F11,    spawn,          {.v = xbacklight_down_tiny }},
    {MODKEY,                        XK_F1,     spawn,          {.v = volume_mute_toggle   }},
    {MODKEY,                        XK_F3,     spawn,          {.v = volume_up            }},
    {MODKEY,                        XK_F2,     spawn,          {.v = volume_down          }},
    {MODKEY|ShiftMask,              XK_F3,     spawn,          {.v = volume_up_tiny       }},
    {MODKEY|ShiftMask,              XK_F2,     spawn,          {.v = volume_down_tiny     }},
    //XF86MonBrightnessDown
    //XF86AudioMute
    //XF86AudioRaiseVolume
    //XF86AudioLowerVolume
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

